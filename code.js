let currentInput = "";
let previousInput = "";
let operator = null;

const box = document.getElementById("box");
const lastOperationHistory = document.getElementById("last_operation_history");

//function to update display
function updateDisplay() {
  box.textContent = currentInput || "0";
  lastOperationHistory.textContent = previousInput + " " + (operator || " ");
}

//function to handle numeric's enter
function button_number(value) {
  if (value === "=") {
    if (currentInput && previousInput && operator) {
      currentInput = evaluate(previousInput, currentInput, operator);
      previousInput = "";
      operator = null;
    }
  } else if (["+", "-", "*", "/"].includes(value)) {
    if (currentInput) {
      if (previousInput && operator) {
        currentInput = evaluate(previousInput, currentInput, operator);
      }
      previousInput = currentInput;
      currentInput = "";
      operator = value;
    }
  } else {
    currentInput += value.toString();
  }
  updateDisplay();
}

//evaluate expressions functions
function evaluate(a, b, op) {
  a = parseFloat(a);
  b = parseFloat(b);
  switch (op) {
    case "+":
      return (a + b).toString();
    case "-":
      return (a - b).toString();
    case "*":
      return (a * b).toString();
    case "/":
      return (a / b).toString();
    default:
      return b;
  }
}

//clear currentInput
function clear_entry() {
  currentInput = "";
  updateDisplay();
}

//clear all
function button_clear() {
  currentInput = "";
  previousInput = "";
  operator = null;
  updateDisplay();
}

//remove backSpace
function backspace_remove() {
  currentInput = currentInput.slice(0, -1);
  updateDisplay();
}

//change sign of currentInput
function plus_minus() {
  currentInput = (parseFloat(currentInput) * -1).toString();
  updateDisplay();
}

//find percentage
function calculate_percentage() {
  currentInput = (parseFloat(currentInput) / 100).toString();
  updateDisplay();
}

//calculate the inverse
function division_one() {
  currentInput = (1 / parseFloat(currentInput)).toString();
  updateDisplay();
}

//find the square of currentInput
function power_of() {
  currentInput = (parseFloat(currentInput) ** 2).toString();
  updateDisplay();
}

//find square root
function square_root() {
  currentInput = Math.sqrt(parseFloat(currentInput)).toString();
  updateDisplay();
}
